@ECHO OFF
ECHO Starting Social Interaction Cloud
docker-compose up dialogflow robot_memory llm
PAUSE
ECHO Stopping Social Interaction Cloud
docker-compose down
PAUSE