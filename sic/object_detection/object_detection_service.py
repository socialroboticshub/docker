from threading import Event, Thread

from detectron2 import model_zoo
from detectron2.config import get_cfg
from detectron2.engine import DefaultPredictor
from numpy import array, argwhere, where, zeros
from sic.image_masks_pb2 import ImageMasks
from sic.service import SICservice
from torch import cuda, set_num_threads

import time
from datetime import datetime

THRESHOLD = 0.7
DPI = 100
MODEL = 'model_final_f10217.pkl'
MODEL_PATH = 'COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml'


class ObjectDetectionService(SICservice):
    def __init__(self, connect, identifier, disconnect):
        self.is_running = False
        self.img_timestamp = None
        self.img_available = Event()

        self.device = 'cuda' if cuda.is_available() else 'cpu'
        set_num_threads(6)

        # Configuration of the detectron predictor
        cfg = get_cfg()
        cfg.merge_from_file(model_zoo.get_config_file(MODEL_PATH))
        cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = THRESHOLD
        cfg.MODEL.DEVICE = self.device
        cfg.TEST.DETECTIONS_PER_IMAGE = DPI
        cfg.MODEL.WEIGHTS = MODEL
        self.predictor = DefaultPredictor(cfg)
        self.calibration_bytes = None

        super().__init__(connect, identifier, disconnect)
        self.events_topic = self.get_full_channel('events')
        self.calibration_topic = self.get_full_channel('image_calibration')
        self.segmentation_topic = self.get_full_channel('segmentation_stream')
        self.object_detected_topic = self.get_full_channel('detected_object')

        self._temp_counter = 0

    def get_device_types(self):
        return ['cam']

    def get_channel_action_mapping(self):
        return {self.get_full_channel('events'): self.execute,
                self.get_full_channel('image_available'): self.set_image_available}

    def counter(self):
        self._temp_counter += 1
        return self._temp_counter - 1

    @staticmethod
    def time_format(time_ms):
        return datetime.fromtimestamp(time_ms / 1000.0).strftime('%Y-%m-%d %H:%M:%S.%f')

    def execute(self, message):
        data = message['data'].decode()
        if data == 'WatchingStarted':
            if not self.is_running:
                self.is_running = True
                if not self.calibration_bytes:
                    self.calibration_bytes = self.redis.get(self.calibration_topic)
                self.update_image_properties()
                detection_thread = Thread(target=self.detect_objects)
                detection_thread.start()
                print(f"{self.__class__.__name__}: {self.counter()} - Start {self.img_timestamp} - {datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')}")
            else:
                print('Object detection already running for ' + self.identifier)
        elif data == 'WatchingDone':
            if self.is_running:
                self.cleanup()
                print('Object detection stopped for ' + self.identifier)
            else:
                print('Object detection already stopped for ' + self.identifier)

    def detect_objects(self):
        self.produce_event('ObjectDetectionStarted')
        while self.is_running:
            if self.img_available.is_set():
                if self.img_timestamp is None:
                    continue

                self.produce_event('SegmentationStarted;' + str(self.img_timestamp))

                left, right = self.retrieve_image(self.img_timestamp, want_stereo=True)
                self.img_available.clear()

                if left is None:
                    continue

                if right is not None and self.calibration_bytes:
                    left, _ = SICservice.rectify_image(self.calibration_bytes, left=left)
                    left = SICservice.crop_image(left)

                output = self.predictor(left)

                # Transfer to cpu and filter on class people
                output_instance = output['instances'].to('cpu')
                persons = output_instance.pred_classes
                person_index = argwhere(persons == 0)[0]
                out = output_instance.pred_masks[person_index].numpy()
                n_masks = out.shape[0]
                if n_masks == 0:
                    out = zeros((1, 1, 0))

                # Create protobuf image_masks
                image_masks = ImageMasks()
                image_masks.timestamp_ms = self.img_timestamp
                image_masks.mask_width = out[0].shape[1]
                image_masks.mask_height = out[0].shape[0]
                image_masks.mask_count = n_masks
                image_masks.masks.extend((out == 1).flatten().tolist())  # to Python array (list) of booleans

                # Create list of centroids of masks
                centroids = []
                if n_masks > 0:
                    for object_mask in out:
                        centroid = array(where(object_mask == 1)).mean(axis=1)  # [y, x]
                        centroids.append((int(centroid[1]), int(centroid[0])))

                # Publish image_masks & centroids
                pipe = self.redis.pipeline()
                pipe.zadd(self.segmentation_topic, {image_masks.SerializeToString(): self.img_timestamp})
                pipe.zremrangebyrank(self.segmentation_topic, 0, -10)
                pipe.publish(self.events_topic, 'SegmentationDone;' + str(self.img_timestamp))
                for centroid in centroids:
                    pipe.publish(self.object_detected_topic, str(centroid[0]) + ';' + str(centroid[1]))
                    print(f"{self.time_format(self.img_timestamp)}: Detected object at ({centroid[0]}, {centroid[1]})")
                pipe.execute()
            else:
                self.img_available.wait()

        self.produce_event('ObjectDetectionDone')

    def set_image_available(self, message):
        raw_timestamp = message['data'].decode()
        self.img_timestamp = int(raw_timestamp)
        self.img_available.set()

    def cleanup(self):
        self.is_running = False
        self.img_available.set()
