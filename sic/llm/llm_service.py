from openai import OpenAI
from simplejson import loads
from sic.service import SICservice


class EntryIncorrectFormatError(Exception):
    """Raised when the received memory entry has an incorrect format"""
    pass


class LLMService(SICservice):
    def __init__(self, connect, identifier, disconnect):
        super().__init__(connect, identifier, disconnect)

    def get_device_types(self):
        return ['robot']

    def get_channel_action_mapping(self):
        return {
            self.get_full_channel('llm_openai_prompt'): self.openai_prompt,
            self.get_full_channel('llm_openai_prompt_agent'): self.openai_prompt_agent,
        }

    def openai_prompt(self, message: str) -> None:
        """

        :param message: OpenAI call parameters:
        {
            prompt_id,
            openai_key,
            prompt_params:
            {
                model,
                message,
                temperature (optional)
            }
        }
        """
        try:
            # Parse message to call parameters dictionary
            params = loads(message['data'].decode())
            # Sometimes loads does not work properly. This is the workaround.
            # if not isinstance(prompt_params, dict):
            #     prompt_params = {}
            #     for item in loads(message['data'].decode()):
            #         prompt_params.update(item)

            # Start connection with OpenAI
            client = OpenAI(api_key=params['openai_key'])
            # Prompt LLM with remaining call parameters
            completion = client.chat.completions.create(**params['prompt_params'])
            # Retrieve result
            result = completion.choices[0].message.content
            # Make sure it is compatible with the redis return format.
            result = result.replace(';', ',')

            # Return result as llm_data.
            self.publish('llm_data', f'{params["prompt_id"]};{result}')
        except KeyError as err:
            print(err)

    def openai_prompt_agent(self, message: str) -> None:
        try:
            prompt_id, prompt_data_raw = self.get_data(message, 2, 'prompt_id;prompt_data')

            prompt_data = loads(prompt_data_raw)
            if not isinstance(prompt_data, dict):
                prompt_data = {}
                for item in loads(prompt_data_raw):
                    prompt_data.update(item)

            print(f' Parsed data for id {prompt_id}: {prompt_data}')

            # Start connection with OpenAI
            client = OpenAI(api_key=prompt_data['openai_key'])
            # Prompt LLM with remaining call parameters
            completion = client.chat.completions.create(
                messages=[{
                    "role": "user",
                    "content": prompt_data['prompt_content'],
                }],
                model="gpt-4o-mini",
            )
            # Retrieve result
            result = completion.choices[0].message.content
            # Make sure it is compatible with the redis return format.
            result = result.replace(';', ',')

            print(f'Result for prompt {prompt_id}: {result}')
            # Return result as llm_data.
            self.publish('llm_data', f'{prompt_id};{result}')

        except(KeyError, EntryIncorrectFormatError) as err:
            print(err)

    @staticmethod
    def get_data(message, correct_length, correct_format=''):
        data = message['data'].decode('utf-8').split(';')
        if len(data) != correct_length:
            raise EntryIncorrectFormatError('Data does not have format ' + correct_format)
        if len(data) == 1:
            return data[0]
        return data
