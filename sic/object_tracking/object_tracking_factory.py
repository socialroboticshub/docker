from sic.factory import SICfactory

from object_tracking_service import ObjectTrackingService


class ObjectTrackingFactory(SICfactory):
    def __init__(self):
        super().__init__()

    def get_connection_channel(self):
        return 'object_tracking'

    def create_service(self, connect, identifier, disconnect):
        return ObjectTrackingService(connect, identifier, disconnect)


if __name__ == '__main__':
    factory = ObjectTrackingFactory()
    factory.run()
