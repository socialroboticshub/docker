from io import BytesIO
from threading import Event, Thread

from face_recognition import face_locations
from numpy import array
from sic.service import SICservice


class PeopleDetectionService(SICservice):
    def __init__(self, connect, identifier, disconnect):
        self.is_detecting = False
        self.save_image = False
        self.img_timestamp = None
        self.img_available = Event()

        super().__init__(connect, identifier, disconnect)

    def get_device_types(self):
        return ['cam']

    def get_channel_action_mapping(self):
        return {self.get_full_channel('events'): self.execute,
                self.get_full_channel('image_available'): self.set_image_available,
                self.get_full_channel('action_take_picture'): self.take_picture}

    def execute(self, message):
        data = message['data'].decode()
        if data == 'WatchingStarted':
            if not self.is_detecting:
                self.is_detecting = True
                self.update_image_properties()
                people_detection_thread = Thread(target=self.detect_people)
                people_detection_thread.start()
            else:
                print('People detection already running for ' + self.identifier)
        elif data == 'WatchingDone':
            if self.is_detecting:
                self.is_detecting = False
                self.img_available.set()
            else:
                print('People detection already stopped for ' + self.identifier)

    def detect_people(self):
        self.produce_event('PeopleDetectionStarted')
        while self.is_detecting:
            if self.img_available.is_set():
                timestamp = self.img_timestamp
                if timestamp is None:
                    continue
                image = self.retrieve_image(timestamp)
                self.img_available.clear()
                if image is None:
                    continue

                if self.save_image:  # If image needs to be saved, publish JPEG back on Redis
                    bytes_io = BytesIO()
                    image.save(bytes_io, 'JPEG')
                    self.publish('picture_newfile', bytes_io.getvalue())
                    self.save_image = False

                # Do the actual detection
                faces = face_locations(array(image))
                for face in faces:  # [top, right, bottom, left]
                    x = int((face[1] + face[3]) / 2)
                    y = int((face[2] + face[0]) / 2)
                    print(self.identifier + ': Detected Person at ' + str(x) + ',' + str(y))
                    self.publish('detected_person', str(x) + ',' + str(y))
            else:
                self.img_available.wait()
        self.produce_event('PeopleDetectionDone')

    def set_image_available(self, message):
        raw_timestamp = message['data'].decode()
        self.img_timestamp = int(raw_timestamp)
        self.img_available.set()

    def take_picture(self, message):
        self.save_image = True

    def cleanup(self):
        self.is_detecting = False
        self.img_available.set()
