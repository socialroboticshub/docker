from sic.factory import SICfactory

from corona_check_service import CoronaCheckService


class CoronaCheckFactory(SICfactory):
    def __init__(self):
        super().__init__()

    def get_connection_channel(self):
        return 'corona_check'

    def create_service(self, connect, identifier, disconnect):
        return CoronaCheckService(connect, identifier, disconnect)


if __name__ == '__main__':
    factory = CoronaCheckFactory()
    factory.run()
