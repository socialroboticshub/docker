from sic.factory import SICfactory

from beamforming_service import BeamformingService


class BeamformingFactory(SICfactory):
    def __init__(self):
        super().__init__()

    def get_connection_channel(self):
        return 'audio_beamforming'

    def create_service(self, connect, identifier, disconnect):
        return BeamformingService(connect, identifier, disconnect)


if __name__ == '__main__':
    factory = BeamformingFactory()
    factory.run()
