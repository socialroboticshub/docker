import cv2
import numpy as np


def ask_input():
    while True:
        txt = input("Satisfied with calibration? (yes/no)")
        if txt == 'yes' or txt == 'no':
            break
        else:
            print("Type yes or no")

    print()
    return txt == 'yes'


def dummy_rectify(shape, mat):
    temp_img = np.ones(shape)
    total = np.sum(temp_img == 1)

    img1_rectified = cv2.warpPerspective(temp_img, mat, temp_img.shape[::-1], borderValue=0)
    return np.sum(img1_rectified == 1) / total * 100


def calibrate_wrapper(est, pepper):
    from PIL import Image, ImageEnhance, ImageOps
    inp = False

    while inp is not True:
        score = 0
        while score < 90:
            img = pepper.get_img()
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            left, right = img[:, :img.shape[1]//2], img[:, img.shape[1]//2:]
            est.calibrate(left, right)
            score = np.mean([dummy_rectify(left.shape, est.H1),
                             dummy_rectify(left.shape, est.H2)])

            print("Score", round(score, 1))

        img1, img2 = est.rectify(left, right)

        # Create anaglyph
        L = Image.fromarray(img1).convert("L")
        L = ImageOps.colorize(L, (0, 0, 0), (0, 255, 255))
        R = Image.fromarray(img2).convert("L")
        R = ImageOps.colorize(R, (0, 0, 0), (255, 0, 0))
        blended = Image.blend(L, R, 0.5)
        enhancer = ImageEnhance.Brightness(blended)
        blended = enhancer.enhance(1.5)
        np_blended = cv2.cvtColor(np.array(blended), cv2.COLOR_RGB2BGR)
        cv2.imshow("Anaglyph", np_blended)

        _ = est.predict(img1, img2)
        cv2.imshow("Left image", img1)
        cv2.imshow("Right image", img2)

        temp = est.disparity_map.copy()
        temp = cv2.normalize(temp, temp, 0, 255, cv2.NORM_MINMAX).astype(np.uint8)
        im_color = cv2.applyColorMap(temp, cv2.COLORMAP_JET)
        cv2.imshow("Disparity map", im_color)
        cv2.waitKey(1)

        inp = ask_input()
    est.store_calibration()

    cv2.waitKey(1)
    cv2.destroyAllWindows()
    cv2.waitKey(1)
    cv2.destroyAllWindows()

    est.predict(img1, img2)
    return
