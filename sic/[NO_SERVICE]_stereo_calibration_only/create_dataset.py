import os
import argparse
from pepper_connector import socket_connection
import cv2
import sys
import tty
from pynput import keyboard
tty.setcbreak(sys.stdin)


def main(args):
    connect = socket_connection(ip=args.ip, port=args.port, camera=args.camera)
    imgs = []

    def on_press(key):
        if key != keyboard.Key.esc:
            print("Image taken")
            imgs.append(connect.get_img())

    def on_release(key):
        if key == keyboard.Key.esc:
            return False

    with keyboard.Listener(on_press=on_press, on_release=on_release) as listener:
        listener.join()

    connect.close_connection()
    save_imgs(imgs, args.save_dir)


def save_imgs(imgs, dir):
    print("Saving images", len(imgs))
    os.makedirs(dir, exist_ok=True)
    os.makedirs(dir + '/left', exist_ok=True)
    os.makedirs(dir + '/right', exist_ok=True)

    for i, img in enumerate(imgs):
        width = img.shape[1]
        left, right = img[:, :width//2, :], img[:, width//2:, :]
        cv2.imwrite(f'{dir}/left/image_{i}.jpg', left)
        cv2.imwrite(f'{dir}/right/image_{i}.jpg', right)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="192.168.0.196",
                        help="Robot IP address.")
    parser.add_argument("--port", type=int, default=12345,
                        help="Port number to retrieve and send data.")
    parser.add_argument("--camera", type=int, default=3,
                        help="Camera preset selection.")
    parser.add_argument("--save_dir", type=str, default="imgs",
                        help="Directory to save images in.")
    args = parser.parse_args()

    main(args)
