# Calibration manual
This manual provides the steps to calibrate a stereo camera. In this manual, Pepper's stereo camera is used as example.

The calibration consists of 5 steps:
- [Setup](#setup)
- [Creating a calibration dataset](#creating-a-calibration-dataset)
- [Independent calibration](#independent-calibration)
- [Stereo calibration](#stereo-calibration)
- [Focal length estimation]() (optional, TODO)

There are two usecases for this calibration manual:

1. You have no (usable) calibration info yet, so you have to start from scratch. Proceed to [Setup](#setup).
2. The camera is already independently calibrated (you already have a .yml file for this or an (old) calibration bytes file). You only want to re-calibrate the stereo part, for example if you use Pepper in a new environment. Proceed to [Stereo calibration](#stereo-calibration).

## Setup
1. Download this chessboard pattern (https://github.com/opencv/opencv/blob/master/doc/pattern.png). Print it as large as possible: preferably A0 size.
2. Start Pepper
3. Copy the file `server.py` to Pepper with for example scp: `scp server.py nao@[IP_PEPPER]:~`.
4. Position Pepper in front of the chessboard. For example: <img src="position.jpeg" alt="Pepper position" width="300"/>

**Important notes on setup**: 
- Make sure that the chessboard is in the center of the image frame and visible to Pepper's both eyes.
- Place the chessboard such that it covers the most of the image frame. In other words, place the board as close to Pepper as possible, while it is still completely in frame for both eyes.
- Make sure there is enough space such that you can move the chessboard around and rotate it.

## Creating a calibration dataset
The following steps are to setup the scripts.
1. Start `server.py` on Pepper with the right camera configuration. First, login to Pepper through ssh: `ssh nao@[IP_PEPPER]` and then `python server.py --cam_id=3 --res_id=[13/14] --freeze`. Use `res_id=13` for `2560*720` resolution and `res_id=14` for `1280*360` resolution.
2. Wait for the script to output `Socket is listening on port: XXXXX` in the terminal.
3. Now, start the generate data script with `sudo python3 create_dataset.py --ip=[IP_PEPPER] --camera=[1/2] --save_dir=imgs`. Use `camera=1` is you used `res_id=14` and use `camera=2` if you used `res_id=13`.
4. The script will take an image if you press any but the escape key.

Now that both scripts are running, follow this steps to actually create the dataset:
1. It is recommended to create about 30 images with all different angles and rotations. For example, if the chessboard can move with 3 degrees of freedom: shift/move position [left, center, right], rotate: [0, -20, 20] and tilt [up, flat, down]. This should yield 27 different images.
2. **Recommended**: take a bit more than 30 images, this will improve calibration.
3. **Important**: the chessboard should be as close as possible to Pepper, but the whole chessboard should be visible to both eyes at all times.
4. For all possible combinations of above positions, set the chessboard in position and press any but the escape key on your keyboard to take the picture.
5. Move the chessboard in a different position, and press any but the escape key again.
6. Repeat this until you have covered all positions.
7. If you are done, press the escape key to stop the script and save the images.

**Important**: now that the images are saved, ensure that the chessboard is *completely* visible in all images.

## Independent calibration
The first step of calibration is to calibrate both cameras independently. Their distortions should be the same hence we calibrate only the left camera.

Follow the following steps to obtain the distortion matrix for the left camera:
1. Measure the width of one square on the chessboard 
2. Count the number of intersections (`= number of squares - 1`) along the chessboard's width and height. For example, a square is 9.1cm and the board has 10 intersections along the height, and 7 along its width.

Now, calibrate the left camera using the images we just made:
```python
python3 first_calibration.py --image_dir=[IMG_DIR/left] --prefix=image --draw --width=7 --height=10 --square_size=9.1 --image_format=jpg --save_file=[FILE_NAME_LEFT.yml]
```

With the `--draw` option, a Python window opens to show the detected corners. \
**Important**: the detected points should be as close as possible to the real intersection of squares in the image. \
**Important**: the script outputs the Root Mean Square (RMS) of the calibration. A RMS lower than 1 should be sufficient. Expect RMS in the interval [0.3, 0.7].

## Stereo calibration
Now that we have an undistortion file, we can perform stereo calibration. This is done using SIFT for more robust calibration. This step is extremely important since it rectifies both images using epipolar geometry.

To perform the stereo calibration, it is recommended to place Pepper in the setting/environment in which it will be used later. Especially since this calibration step might be performed again if the setting changes.  

First, make sure `server.py` is running on Pepper again, for example: `python server.py --freeze --cam_id=3 --res_id=14`.

Running the stereo calibration using the K D matrices (from the previous step, or some old matrices):
```python
python3 second_calibration.py --ip=[IP_PEPPER] --camera=[1/2] --K_D_matrix=[CALIB_FILE.yml] [--suffix=[SOME_SUFFIX_FOR_FILENAME]]
```

Using some old calibration info file:
```python
python3 second_calibration.py --ip=[IP_PEPPER] --camera=[1/2] --calibration_bytes=[CALIB_FILE] [--suffix=[SOME_SUFFIX_FOR_FILENAME]]
```

Choose the same value for `--camera` here as you did while creating the dataset.\
The `--K_D_matrix` should point to the file as created during the independent calibration step, or the old file you want to use.\
The `--calibration_bytes` should point to a calibration info byte file from which you want to use the K D matrices. 

The script will show you example calibrations and asks you if you are satisfied with the calibration.
A good calibration shows images that are not warped to strongly and ensures that a point in the left image lies on a straight line in the right image. 

Finally, the script will save one file `calibration_info_stereo_[IMG_WIDTH]_[IMG_HEIGHT]_[TIME_STRING][SUFFIX]` which is a base64 bytestring that contains all necessary calibration information in the `stored_calibrations` directory.

You can copy this file to Pepper to use it in SIC (do need to rename the file to `calibration` on Pepper then).