import numpy as np
import cv2
import codecs
import pickle
import argparse


class StereoDepth():
    def __init__(self, calibration_bytes=None, K_D_matrix=None, img_shape=None, block_size=10, min_disp=-16, num_disp=128,
                 uniquenessRatio=5, specklewind=200, speckle=2,
                 disp12MaxDiff=0, max_dist_vis=1000, focal_length=50, cam_dist=78, disp_offset=150, filename_suffix=''):

        self.block_size = block_size
        self.min_disp = min_disp
        self.num_disp = num_disp
        self.uniquenessRatio = uniquenessRatio
        self.specklewind = specklewind
        self.speckle = speckle
        self.disp12MaxDiff = disp12MaxDiff
        self.max_dist_vis = int(max_dist_vis)
        self.cmap = cv2.COLORMAP_MAGMA
        self.image_shape = img_shape

        self.focal_length = focal_length
        self.cam_dist = cam_dist
        self.disp_offset = disp_offset

        self.stereo_sgbm, self.disparity_map = None, None
        self.H1, self.H2 = None, None

        self.filename_suffix = filename_suffix
        self.read_calibration(calibration_bytes, K_D_matrix)

        self.left_matcher = cv2.StereoSGBM_create(
            minDisparity=self.min_disp,
            numDisparities=self.num_disp,
            blockSize=self.block_size,
            uniquenessRatio=self.uniquenessRatio,
            speckleWindowSize=self.specklewind,
            speckleRange=self.speckle,
            disp12MaxDiff=self.disp12MaxDiff,
            P1=8 * 1 * self.block_size * self.block_size,
            P2=32 * 1 * self.block_size * self.block_size,
            mode=3
        )
        self.right_matcher = cv2.ximgproc.createRightMatcher(self.left_matcher)
        self.wls_filter = cv2.ximgproc.createDisparityWLSFilter(self.left_matcher)
        self.wls_filter.setLambda(1000.0)
        self.wls_filter.setSigmaColor(1.5)

    def read_calibration(self, calibration_bytes, K_D_matrix):
        if not calibration_bytes and K_D_matrix:
            return self.init_undistort(K_D_matrix)

        from pickle import loads
        from codecs import decode

        with open(calibration_bytes, 'rb') as f:
            cameramatrix, K, D, H1, H2 = loads(decode(f.read(), 'base64'))

        self.newcameramtx = cameramatrix
        self.K = K
        self.D = D
        self.H1 = H1
        self.H2 = H2

    def init_undistort(self, path):
        fs = cv2.FileStorage(path, cv2.FILE_STORAGE_READ)
        self.K = fs.getNode("K").mat()
        self.D = fs.getNode("D").mat()

        self.newcameramtx, self.roi = cv2.getOptimalNewCameraMatrix(self.K, self.D, self.image_shape, 1, self.image_shape)

    def undistort_img(self, img):
        top = int(30 / 360 * img.shape[0])
        left = int(104 / 640 * img.shape[1])
        right = int(-66 / 640 * img.shape[1])
        return cv2.undistort(img, self.K, self.D, None, self.newcameramtx)[top:, left:right]

    def get_distance_map(self):
        if self.disparity_map is None:
            return np.zeros((1, 1))

        dist_map = self.disp_to_cm(self.disparity_map)
        dist_map[dist_map < 0] = 0
        dist_map[dist_map > self.max_dist_vis] = self.max_dist_vis
        return dist_map

    def disp_to_cm(self, disp):
        return ((self.focal_length * self.cam_dist) / (disp + 1e-10))

    def store_calibration(self):
        calibration_info = (self.newcameramtx, self.K, self.D, self.H1, self.H2)
        pickled = codecs.encode(pickle.dumps(calibration_info), "base64")  # Store as byte string

        import time
        timestr = time.strftime("%Y%m%d_%H%M%S")
        with open(f'calibration_info_{self.image_shape[0]}_{self.image_shape[1]}_{timestr}{self.filename_suffix}', 'wb') as f:
            f.write(pickled)

    def calibrate(self, img1: np.ndarray, img2: np.ndarray):
        assert img1.ndim == 2 and img2.ndim == 2, f"Use grayscale images! Got {img1.ndim, img1.shape}"
        img1 = self.undistort_img(img1)
        img2 = self.undistort_img(img2)

        # Initiate SIFT detector
        sift = cv2.SIFT_create()

        # find the keypoints and descriptors with SIFT
        kp1, des1 = sift.detectAndCompute(img1, None)
        kp2, des2 = sift.detectAndCompute(img2, None)

        # Match keypoints in both images
        # Based on: https://docs.opencv.org/master/dc/dc3/tutorial_py_matcher.html
        FLANN_INDEX_KDTREE = 0
        index_params = dict(algorithm=FLANN_INDEX_KDTREE, trees=50)
        search_params = dict(checks=1000)
        flann = cv2.FlannBasedMatcher(index_params, search_params)
        matches = flann.knnMatch(des1, des2, k=2)

        # Keep good matches: calculate distinctive image features
        # https://www.cs.ubc.ca/~lowe/papers/ijcv04.pdf
        pts1 = []
        pts2 = []

        for i, (m, n) in enumerate(matches):
            if m.distance < 0.6 * n.distance:
                # Keep this keypoint pair
                pts2.append(kp2[m.trainIdx].pt)
                pts1.append(kp1[m.queryIdx].pt)

        pts1 = np.int32(pts1)
        pts2 = np.int32(pts2)
        print("Using", len(pts1), "points for calibration.")
        fundamental_matrix, inliers = cv2.findFundamentalMat(pts1, pts2,
                                                             cv2.FM_RANSAC, 1,
                                                             0.999)
        # We select only inlier points
        if inliers is None:
            print("Calibration failed. Try again")
            exit(1)

        print(f"Calibration uses {round(len(pts1[inliers.ravel() == 1]) / len(pts1) * 100, 1)}% inliers")
        pts1 = pts1[inliers.ravel() == 1]
        pts2 = pts2[inliers.ravel() == 1]

        _, H1, H2 = cv2.stereoRectifyUncalibrated(np.float32(pts1),
                                                  np.float32(pts2),
                                                  fundamental_matrix,
                                                  imgSize=img1.shape)
        self.H1 = H1
        self.H2 = H2

    def rectify(self, img1, img2):
        assert self.H1 is not None and self.H2 is not None, "Calibrate first with self.calibrate()"

        img1 = self.undistort_img(img1)
        img2 = self.undistort_img(img2)

        img1_rectified = cv2.warpPerspective(img1, self.H1, img1.shape[::-1])
        img2_rectified = cv2.warpPerspective(img2, self.H2, img2.shape[::-1])

        return img1_rectified, img2_rectified

    def predict(self, img1, img2):
        left_disp = self.left_matcher.compute(img1, img2)
        right_disp = self.right_matcher.compute(img2, img1)
        self.left_disp = left_disp
        self.right_disp = right_disp

        filtered_disp = self.wls_filter.filter(left_disp, img1, disparity_map_right=right_disp)
        filtered_disp += self.disp_offset

        self.disparity_map = filtered_disp.astype(np.float32)
        self.disparity_map /= 16  # Disparity must always be divided by 16


def main(args):
    from utils import calibrate_wrapper
    from pepper_connector import socket_connection
    HIGH_RES_SHAPE = (2560, 720)

    pepper = socket_connection(args.ip, args.port, args.camera)
    img_shape = (pepper.width, pepper.height)
    # img_shape = (HIGH_RES_SHAPE[0] / 2, HIGH_RES_SHAPE[1] / 2)

    config = {
        "block_size": 10 if img_shape == HIGH_RES_SHAPE else 6,
        "min_disp": -16,
        "num_disp": 128 if img_shape == HIGH_RES_SHAPE else 80,
        "uniquenessRatio": 5,
        "specklewind": 200,
        "speckle": 2,
        "disp12MaxDiff": 0,
        "max_dist_vis": 5 * 100,
        "img_shape": img_shape,
        "calibration_bytes": args.calibration_bytes,
        "filename_suffix": args.suffix,
        "K_D_matrix": args.K_D_matrix
    }

    depth_estimator = StereoDepth(**config)
    calibrate_wrapper(depth_estimator, pepper)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="192.168.0.196",
                        help="Robot IP address.")
    parser.add_argument("--port", type=int, default=12345,
                        help="Port number to retrieve and send data.")
    parser.add_argument("--camera", type=int, default=3,
                        help="Camera preset selection.")
    parser.add_argument("--calibration_bytes", type=str, default='',
                        help="Path to calibration file")
    parser.add_argument("--K_D_matrix", type=str, default='',
                        help="Path to K D matrix file (.yml), not used if --calibration_bytes is given")
    parser.add_argument("--suffix", type=str, default='',
                        help="Suffix added to calibration filename")
    args = parser.parse_args()

    main(args)
